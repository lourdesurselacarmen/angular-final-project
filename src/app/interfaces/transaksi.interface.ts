export interface TransaksiUser {
    idTransaction: number
    idMember: number
    paymentDeadline: string
    transactionValue: number
    dateOfTransaction: string
    is_paid: boolean
}

export interface MemberTransactionList {
    memberName: string
    status: string
    roleMember: boolean
    transactions: TransaksiUser
}

export interface TransaksiUserMember {
    transactions: TransaksiUser[]
}

export interface TransaksiGrup {
    "idGroupTransaction": number
    "idGroup": number
    "winner": string
    "moneyTransfered": number
    "dateOfGroupTransaction": string
    "transfered": boolean
}

export interface Dompet {
    idWallet: number
    groupWalletBalance: number
}