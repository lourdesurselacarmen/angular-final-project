export interface ResponseBase<T> {
    status: boolean
    message: string
    data: T
}
