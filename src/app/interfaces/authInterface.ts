export interface RequestRegister {
    fullName: string
    username: string
    phoneNumber: string
    dateOfBirth: string
    gender: string
    password: string
    repeatPass: string
    // photo?: string
}

export interface ResponseRegister {
    idUser: number
    fullName: string
    username: string
    phoneNumber: string
    dateOfBirth: string
    gender: string
    admin: boolean
}

export interface ResponseLogin {
    token: string
    role: string
    username: string
    jwttoken: string
}

export interface RequestLogin {
    username: string
    password: string
}