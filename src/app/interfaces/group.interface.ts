export interface RequestRegisterGroup {
  groupName: string;
  createdDate: string;
  passcode: string;
}

export interface ResponseRegisterGroup {
  idGroup: number;
  groupName: string;
  createdDate: string;
  totalMoney: number;
  totalMember: number;
}

export interface Member {
  memberName: string;
  status: string;
  roleMember: boolean;
}

export interface MemberTransaction {
  memberName: string;
  status: string;
  roleMember: boolean;
  transaction: [
    {
      paymentDeadline: string;
      transactionValue: number;
      dateOfTransaction: string;
      is_paid: boolean;
    }
  ];
}

export interface GroupTransaction {
  idGroup: number;
  groupName: string;
  totalMember: number;
  createdDate: string;
  totalMoney: number;
  groupWallet: {
    idWallet: number;
    groupWalletBalance: number;
  };
  member: [
    {
      memberName: string;
      status: string;
      roleMember: boolean;
    }
  ];
  groupTransaction: [
    {
      winner: string;
      moneyTransfered: number;
      dateOfGroupTransaction: string;
      transfered: boolean;
    }
  ];
}

export interface GroupAdmin {
  idGroup: number;
  groupName: string;
  totalMember: number;
  createdDate: string;
  totalMoney: number;
  member: [
    {
      memberName: string;
      status: string;
      roleMember: boolean;
    }
  ];
}

export interface RequestChangeAdmin {
  memberName: string;
}

export interface JumlahTransaksiGrup {
  winner: string;
  moneyTransfered: number;
  dateOfGroupTransaction: string;
  transfered: boolean;
}
