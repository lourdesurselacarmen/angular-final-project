import { Component, EventEmitter, Input, Output } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-calculate-gasoline',
  templateUrl: './calculate-gasoline.component.html',
  styleUrls: ['./calculate-gasoline.component.scss']
})
export class CalculateGasolineComponent {

  TestClass = 'alert alert-success'

  harga = 0

  constructor(private dataService: DataService) {
    this.harga = this.dataService.baseHarga
  }
  //buat ambil harga dari service ke dalam kelas

  hargaPerLiter = 0

  liter = 0
  uang = 0
  
  total: number = 0
  kembalian: number = 0
  status = ''

  showChange = false

  doClick(jenis: string) {
    switch (jenis) {
      case 'pertamax':
        this.hargaPerLiter = 13900
        break
      case 'pertalite':
        this.hargaPerLiter = 10000
        break
      case 'pertamina dex':
        this.hargaPerLiter = 18550
        break
      case 'dex lite':
        this.hargaPerLiter = 18000
        break
      default:
        this.hargaPerLiter = 0
    }
  }

  hitung(literParam: number, uangParam: number) {
    this.showChange = true
    this.total = this.hargaPerLiter*literParam + this.harga
    this.kembalian = uangParam - this.total
    if (this.kembalian>0) {
      this.TestClass = 'alert alert-success' 
      this.status = 'Anda berhasil melakukan pembayaran'
    } else {
      this.TestClass = 'alert alert-danger'
      this.status = 'Cek kembali uang yang anda berikan, uang anda tidak mencukupi untuk melakukan transaksi'
    }
  }
}
