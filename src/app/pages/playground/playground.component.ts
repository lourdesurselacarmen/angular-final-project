import { Component } from '@angular/core';

@Component({
  selector: 'app-playground',
  templateUrl: './playground.component.html',
  styleUrls: ['./playground.component.scss']
})

export class PlaygroundComponent {
  title = 'ng-batch-four-carmen'

  //data: string = 'value';
  //data: number = 0

  name = 'Lourdes Ursela Carmen'
  age = 22
  status = false

  dataArr = [1,2,3]
  //kalau mau buat data yang banyak bisa dengan bantuan library

  dataArrNew = new Array(10)

  person = {
    title: 'Coba A',
    name: 'lourdes',
    age: 19,
    status: true
  }
  //buat objek

  showData = false

  swicthValue = 6
  
  personList = [
    {
    title: 'Data List',
    name: 'Lourdes',
    age: 22,
    status: true
    },
    {
      title: 'Data List 2',
      name: 'Ursela',
      age: 27,
      status: true 
    },
  ]

  constructor() {
    this.name = 'Carmen'
    //ini assign data lagi tapi hrus 1 tipe data yang sama
    this.age = 23
  }

  onCallBack(ev: any) {
    console.log(ev)
    this.personList.push(ev.data)
    //ini untuk push data dari oncallback ke dalam list personList
  }
  //untuk menampung fungsi dalam click
}
