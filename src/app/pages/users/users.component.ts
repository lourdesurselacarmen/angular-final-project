import { Component } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { User } from 'src/app/interfaces/interface';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent {
  users!: User[]
  //atau bisa juga users: User[] = []

  constructor(private dataService: DataService) {

    this.dataService.getUser().subscribe(
      response => {
        console.log(response)
        this.users = response
        //response ini untuk nampung dulu data yang di get
      }
    )
    //untuk nangkep httpclient dia pake method subscribe karena tipe si httpclient ini observable

  }
}
