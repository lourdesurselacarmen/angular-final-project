import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as dayjs from 'dayjs';
import { map, Subject, switchMap, takeUntil } from 'rxjs';
import { ResponseRegister } from 'src/app/interfaces/authInterface';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent {

  usernameLog!: string
  userProfile!: ResponseRegister
  formProfile: FormGroup
  refresh = new Subject<void>();

  constructor(private userService: UserService, private authService: AuthService, private formBuilder: FormBuilder) {
    this.formProfile = this.formBuilder.group({
      idUser: [''],
      username: ['', [Validators.required]],
      fullName: ['', [Validators.required]],
      gender: ['', [Validators.required]],
      dateOfBirth: ['', [Validators.required]]
    })

    this.userService.getUserProfile().subscribe(
      response => {
        console.log(response)
        this.userProfile = response

        this.formProfile.controls['idUser'].patchValue(this.userProfile.idUser)
        this.formProfile.controls['fullName'].patchValue(this.userProfile.fullName)
        this.formProfile.controls['username'].patchValue(this.userProfile.username)
        this.formProfile.controls['dateOfBirth'].patchValue(this.userProfile.dateOfBirth)
        this.formProfile.controls['gender'].patchValue(this.userProfile.gender)
        console.log(this.formProfile)
      }
    )
  }

  get errorControl() {
    return this.formProfile.controls
  }

  loadData() {
    this.userService
      .getUserProfile()
      .pipe(takeUntil(this.refresh))
      .subscribe((response) => {
        console.log(response);
        this.userProfile = response;
      });
  }

  doChangeProfile() {
    this.userService.getUserProfile().pipe(
          switchMap((val) => {
            const payload = {
              fullName: this.formProfile.value.fullName,
              dateOfBirth: dayjs(this.formProfile.value.dateOfBirth).format('YYYY-MM-DD'),
              gender: this.formProfile.value.gender
            }
            return this.userService.updateProfil(this.userProfile.idUser, payload).pipe(
              map(val => val)
            )
          })
        ).subscribe(response => {
          Swal.fire({
            title: 'Success!',
            text: 'Anda berhasil mengubah profil anda',
            icon: 'success',
            confirmButtonText: 'OK'
          })
          console.log(response)
          this.loadData();
        })
  }

}
