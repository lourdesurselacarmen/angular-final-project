import { Component } from '@angular/core';
import { TransaksiGrup } from 'src/app/interfaces/transaksi.interface';
import { HistoryGrupService } from 'src/app/services/history-grup.service';

@Component({
  selector: 'app-history-transaksi-group',
  templateUrl: './history-transaksi-group.component.html',
  styleUrls: ['./history-transaksi-group.component.scss']
})
export class HistoryTransaksiGroupComponent {
  transaksiGrup: TransaksiGrup[] = [] 

  constructor(private historyGrupService: HistoryGrupService) {
    this.historyGrupService.getlistTransaksi().subscribe(
      response => {
        console.log(response)
        this.transaksiGrup = response
        //response ini untuk nampung dulu data yang di get
      }
    )
  }
}
