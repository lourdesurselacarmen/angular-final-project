import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoryTransaksiGroupComponent } from './history-transaksi-group.component';

describe('HistoryTransaksiGroupComponent', () => {
  let component: HistoryTransaksiGroupComponent;
  let fixture: ComponentFixture<HistoryTransaksiGroupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistoryTransaksiGroupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HistoryTransaksiGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
