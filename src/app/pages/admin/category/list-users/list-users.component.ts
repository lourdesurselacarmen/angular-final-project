import { Component, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';
import { map } from 'rxjs';
import { FormBuilder } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';
import { Userdata } from 'src/app/interfaces/user.interface';
import { TransaksiUser } from 'src/app/interfaces/transaksi.interface';
import { MemberTransaction, ResponseRegisterGroup } from 'src/app/interfaces/group.interface';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.scss']
})
export class ListUsersComponent {
  modalRef!: BsModalRef
  users: Userdata[] = []
  members: MemberTransaction[] = []
  groupList: Userdata[] = []
  groups: ResponseRegisterGroup[] = []
  photo!: string
  photoFile!: File
  formRegister: FormGroup
  formUpdateUser: FormGroup
  userDetail!: Userdata
  transaksiUser: any
  transaksiUserDetail: TransaksiUser[] = []

  constructor(private userService: UserService, private modalService: BsModalService, private authService: AuthService, private formBuilder: FormBuilder) {

    this.userService.getlistUser().subscribe(
      response => {
        console.log(response)
        this.users = response
        //response ini untuk nampung dulu data yang di get
      }
    )

    this.formRegister = this.formBuilder.group({
      username: ['', [Validators.required]],
      fullName: ['', [Validators.required]],
      phoneNumber: ['', [Validators.required, Validators.minLength(8)]],
      dateOfBirth: ['', [Validators.required]],
      gender: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      repeatPass: ['', [Validators.required, Validators.minLength(8)]],
      admin: ['', [Validators.required]]
    })

    this.formUpdateUser = this.formBuilder.group({
      idUser: [''],
      fullName: [''],
      username: [''],
      password: ['', [Validators.required, Validators.minLength(8)]],
      phoneNumber: [''],
      dateOfBirth: [''],
      gender: [''],
      admin: ['']
    })
  }

  get errorControl() {
    return this.formRegister.controls
    //untuk handling error dri regex
  }

  get errorControlUpdate() {
    return this.formUpdateUser.controls
  }


  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template)
  }

  doShowUserTransaction(data: any) {
    console.log(data)
    this.members = data.member
    this.userService.getlistTransaksi(data.username).pipe(
      map(val => val)
    ).subscribe(
      response => {
        console.log(response)
        this.transaksiUser = response
        // response.forEach(val => {
          // console.log("val" + val)
          // if (val.transactions.length != 0) {
          //   val.transactions.forEach(val2 => {
          //     this.transaksiUserDetail.push(
          //       {
          //         "idTransaction": val2.idTransaction,
          //         "idMember": val2.idMember,
          //         "paymentDeadline": val2.paymentDeadline,
          //         "transactionValue": val2.transactionValue,
          //         "dateOfTransaction": val2.dateOfTransaction,
          //         "is_paid": val2.is_paid
          //       }
          //     )
          //   });
          // }
        // })
        // console.log(this.transaksiUserDetail)
      }
    )
  }

  doShowGroup(data: any) {
    console.log(data)
    this.groupList = data
    this.userService.getlistGroup(data.username).pipe(
      map(val => val)
    ).subscribe(
      response => {
        console.log(response)
        this.groups = response
      }
    )
  }

  // showPreview(event: any) {
  //   if(event) {
  //     const file = event.target.files[0]
  //     //buat ambil gambarnya
  //     this.photoFile = file
  //     //nampung file
  //     const reader = new FileReader()
  //     //buat bacanya
  //     reader.onload = () => {
  //       this.photo = reader.result as string
  //     }
  //     reader.readAsDataURL(file)
  //   }
  // }

  // doAddUser() {
  //   this.userService.uploadPhoto(this.photoFile).pipe(
  //     switchMap((val) => {
  //       const payload = {
  //         fullName: this.formRegister.value.fullName,
  //         username: this.formRegister.value.username,
  //         phoneNumber: this.formRegister.value.phoneNumber,
  //         dateOfBirth: dayjs(this.formRegister.value.dateOfBirth).format('YYYY-MM-DD'),
  //         gender: this.formRegister.value.gender,
  //         password: this.formRegister.value.password,
  //         repeatPass: this.formRegister.value.repeatPass,
  //         // photo: val.data.image
  //       }
  //       return this.authService.register(payload).pipe(
  //         map(val => val)
  //       )
  //     })
  //   ).subscribe(response => console.log(response))
  // }

  // doShowData(data: any) {
  //   console.log(data)
  //   this.userDetail = data

  //   this.formUpdateUser.controls['fullName'].patchValue(data.fullName)
  //   this.formUpdateUser.controls['username'].patchValue(data.username)
  //   this.formUpdateUser.controls['password'].patchValue(data.password)
  //   this.formUpdateUser.controls['phoneNumber'].patchValue(data.phoneNumber)
  //   this.formUpdateUser.controls['dateOfBirth'].patchValue(data.dateOfBirth)
  //   this.formUpdateUser.controls['gender'].patchValue(data.gender)
  //   this.formUpdateUser.controls['admin'].patchValue(data.admin)
  //   console.log(this.formUpdateUser)
  // }

  // doChangePassword() {
  //   this.userService.getlistUser().pipe(
  //     switchMap((val) => {
  //       const payload = {
  // fullName: this.formUpdateUser.value.fullName,
  // username: this.formUpdateUser.value.username,
  // password: this.formUpdateUser.value.password
  // phoneNumber: this.formUpdateUser.value.phoneNumber,
  // dateOfBirth: dayjs(this.formUpdateUser.value.dateOfBirth).format('YYYY-MM-DD'),
  // gender: this.formUpdateUser.value.gender,
  // admin: this.formUpdateUser.value.admin
  //       }
  //       return this.userService.updatePassword(this.userDetail.idUser, payload).pipe(
  //         map(val => val)
  //       )
  //     })
  //   ).subscribe(response => {
  //     Swal.fire({
  //       title: 'Success!',
  //       text: 'Anda berhasil mengubah password user',
  //       icon: 'success',
  //       confirmButtonText: 'OK'
  //     })
  //     console.log(response)
  //   })
  // }
}
