import { Component, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { FormBuilder } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { GroupService } from 'src/app/services/group.service';
import { AuthService } from 'src/app/services/auth.service';
import { Validators } from '@angular/forms';
import { switchMap, map, Subject, takeUntil } from 'rxjs';
import * as dayjs from 'dayjs';
import Swal from 'sweetalert2';
import { GroupTransaction, Member } from 'src/app/interfaces/group.interface';
import {
  Dompet,
  TransaksiGrup
} from 'src/app/interfaces/transaksi.interface';

@Component({
  selector: 'app-list-groups',
  templateUrl: './list-groups.component.html',
  styleUrls: ['./list-groups.component.scss'],
})
export class ListGroupsComponent {
  modalRef!: BsModalRef;
  groups: GroupTransaction[] = [];
  group2: GroupTransaction[] = [];
  groupDompet: GroupTransaction[] = [];
  memberGroup: Member[] = [];
  transaksiGrup: TransaksiGrup[] = [];
  walletGrup!: Dompet;
  // photo!: string;
  // photoFile!: File;
  formRegisterGroup: FormGroup;
  formChangeAdmin: FormGroup;
  totalMoney!: number;
  groupList!: GroupTransaction;
  groupList2!: GroupTransaction;
  refresh = new Subject<void>();

  members = {
    memberName: '',
    status: '',
    roleMember: false,
  };

  constructor(
    private groupService: GroupService,
    private modalService: BsModalService,
    private formBuilder: FormBuilder
  ) {
    this.loadData();
    this.formRegisterGroup = this.formBuilder.group({
      groupName: ['', [Validators.required]],
      passcode: ['', [Validators.required, Validators.minLength(8)]],
      createdDate: ['', [Validators.required]],
    });

    this.formChangeAdmin = this.formBuilder.group({
      memberName: ['', [Validators.required]],
    });

    this.groupService.getlistGroup().subscribe((response) => {
      console.log(response);
      this.groups = response;
      //response ini untuk nampung dulu data yang di get
    });
  }

  loadData() {
    this.groupService
      .getlistGroup()
      .pipe(takeUntil(this.refresh))
      .subscribe((response) => {
        console.log(response);
        this.groups = response;
      });
  }

  get errorControl() {
    return this.formRegisterGroup.controls;
    //untuk handling error dri validators
  }

  get errorControlAdmin() {
    return this.formChangeAdmin.controls;
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  doClick(money: number) {
    switch (money) {
      case 500000:
        this.totalMoney = 500000;
        break;
      case 1000000:
        this.totalMoney = 1000000;
        break;
      case 2000000:
        this.totalMoney = 2000000;
        break;
      case 3000000:
        this.totalMoney = 3000000;
        break;
      default:
        this.totalMoney = 0;
    }
  }

  doAddGroup() {
    this.groupService
      .getlistGroup()
      .pipe(
        switchMap((val) => {
          const payload = {
            groupName: this.formRegisterGroup.value.groupName,
            passcode: this.formRegisterGroup.value.passcode,
            createdDate: dayjs(this.formRegisterGroup.value.createdDate).format(
              'YYYY-MM-DD'
            ),
          };
          return this.groupService
            .registerGroup(this.totalMoney, payload)
            .pipe(map((val) => val));
        })
      )
      .subscribe((response) => {
        Swal.fire({
          title: 'Success!',
          text: 'Anda berhasil menambahkan akun!',
          icon: 'success',
          confirmButtonText: 'OK',
        });
        console.log(response);
        this.loadData();
      });
  }

  doShowIDGroup(data: any) {
    console.log(data);
    this.groupList = data;
    this.groupService
      .getMemberList(data.groupName)
      .pipe(map((val) => val))
      .subscribe((response) => {
        console.log(response);
        this.memberGroup = response;
      });
  }

  doShowGroupTransaction(data: any) {
    console.log(data);
    this.transaksiGrup = data.groupTransactions;
  }

  doShowWallet(data: any) {
    console.log(data);
    this.groupDompet = data;
    this.walletGrup = data.groupWallet;
  }

  doShowGroupInfo(data: any) {
    console.log(data);
    this.groupList2 = data;
  }

  doChangeAdmin() {
    this.groupService
      .getlistGroup()
      .pipe(
        switchMap((val) => {
          const payload = {
            memberName: this.formChangeAdmin.value.memberName,
          };
          return this.groupService
            .changeAdminGroup(this.groupList2.groupName, payload)
            .pipe(map((val) => val));
        })
      )
      .subscribe((response) => {
        Swal.fire({
          title: 'Success!',
          text: 'Anda berhasil mengubah admin grup ini!',
          icon: 'success',
          confirmButtonText: 'OK',
        });
        console.log(response);
        this.loadData();
      });
  }
}
