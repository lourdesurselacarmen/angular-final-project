import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoryTransaksiUserComponent } from './history-transaksi-user.component';

describe('HistoryTransaksiUserComponent', () => {
  let component: HistoryTransaksiUserComponent;
  let fixture: ComponentFixture<HistoryTransaksiUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistoryTransaksiUserComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HistoryTransaksiUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
