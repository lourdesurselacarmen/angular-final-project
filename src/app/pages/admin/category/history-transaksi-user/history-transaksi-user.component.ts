import { Component, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { map } from 'rxjs';
import { MemberTransaction } from 'src/app/interfaces/group.interface';
import { MemberTransactionList } from 'src/app/interfaces/transaksi.interface';
import { HistoryUserService } from 'src/app/services/history-user.service';

@Component({
  selector: 'app-history-transaksi-user',
  templateUrl: './history-transaksi-user.component.html',
  styleUrls: ['./history-transaksi-user.component.scss']
})
export class HistoryTransaksiUserComponent {
  transaksiUser: MemberTransactionList[] = [] 
  modalRef!: BsModalRef
  memberList!: MemberTransaction

  constructor(private historyUserService: HistoryUserService, private modalService: BsModalService) {
    this.historyUserService.getlistTransaksi().pipe(
      map(val => val)
    ).subscribe(
      response => {
        console.log(response)
        this.transaksiUser = response
      }
    )
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template)
  }

  // doShowTransaction(data: any) {
  //   console.log(data)
  //   this.memberList = data
  //   this.historyUserService.getlistTransaksi().pipe(
  //     map(val => val)
  //   ).subscribe(
  //     response => {
  //       console.log(response)
  //       this.transaksiUser = response
  //     }
  //   )
  // }
}
