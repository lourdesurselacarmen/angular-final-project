import { Component } from '@angular/core';
import { GroupTransaction, JumlahTransaksiGrup } from 'src/app/interfaces/group.interface';
import { Userdata } from 'src/app/interfaces/user.interface';
import { DashboardService } from 'src/app/services/dashboard.service';
import { GroupService } from 'src/app/services/group.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {
  users: Userdata[] = []
  groups: GroupTransaction[] = []
  jumlahTransaksi: JumlahTransaksiGrup[] = []
  transactionValue!: number
  revenue!: number

  constructor(private userService: UserService, private dashboardService: DashboardService, private groupService: GroupService) {
    this.userService.getlistUser().subscribe(
      response => {
        console.log(response)
        this.users = response
      }
    )

    this.dashboardService.getDashboardTransaction().subscribe(
      response => {
        console.log(response)
        this.transactionValue = response
      }
    )

    this.dashboardService.getDashboardRevenue().subscribe(
      response => {
        console.log(response)
        this.revenue = response
      }
    )

    this.groupService.getlistGroup().subscribe(
      response => {
        console.log(response)
        this.groups = response
      }
    )

    this.groupService.getAllTransaction().subscribe(
      response => {
        console.log(response)
        this.jumlahTransaksi = response
      }
    )
  }
}
