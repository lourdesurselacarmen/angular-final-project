import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { RouterModule, Routes } from '@angular/router';
import { LayoutsModule } from 'src/app/layouts/layouts.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SettingsComponent } from './settings/settings.component';
import { ListUsersComponent } from './category/list-users/list-users.component';
import { HistoryTransaksiUserComponent } from './category/history-transaksi-user/history-transaksi-user.component';
import { HistoryTransaksiGroupComponent } from './category/history-transaksi-group/history-transaksi-group.component';
import { ListGroupsComponent } from './category/list-groups/list-groups.component';
import { ProfileComponent } from './category/profile/profile.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'settings',
        component: SettingsComponent
      },
      {
        path: 'category/list-users',
        component: ListUsersComponent
      },
      {
        path: 'category/list-groups',
        component: ListGroupsComponent
      },
      {
        path:'category/history-transaksi-user',
        component: HistoryTransaksiUserComponent
      },
      {
        path:'category/history-transaksi-group',
        component: HistoryTransaksiGroupComponent
      },
      {
        path:'category/profile',
        component: ProfileComponent
      }
    ]
  }
]

@NgModule({
  declarations: [AdminComponent, DashboardComponent, SettingsComponent, ListUsersComponent, HistoryTransaksiUserComponent, HistoryTransaksiGroupComponent, ListGroupsComponent, ProfileComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    LayoutsModule,
    ModalModule.forRoot(),
    FormsModule,
    ReactiveFormsModule
  ]
})
export class AdminModule { }
