import { LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CardComponent } from './components/card/card.component';
import { LoginComponent } from './pages/login/login.component';
import { PlaygroundComponent } from './pages/playground/playground.component';
// import { RegisterComponent } from './pages/register/register.component';
import { CalculateGasolineComponent } from './pages/calculate-gasoline/calculate-gasoline.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { UsersComponent } from './pages/users/users.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { InterceptorInterceptor } from './core/interceptor.interceptor';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { registerLocaleData } from '@angular/common';
import localId from '@angular/common/locales/id'

registerLocaleData(localId, 'id')

@NgModule({
  declarations: [
    AppComponent,
    CardComponent,
    LoginComponent,
    PlaygroundComponent,
    // RegisterComponent,
    CalculateGasolineComponent,
    UsersComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    //ini buat ngebuat si input dan outputnya jadi dinamis (bisa merubah variabel di dalam html dengan ng model)
    HttpClientModule,
    ModalModule.forRoot(),
    BsDropdownModule.forRoot()
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorInterceptor,
      multi: true
    },
    {
      provide: LOCALE_ID,
      useValue: 'id'
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
