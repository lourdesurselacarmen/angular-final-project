import { Injectable } from '@angular/core';
import { CanActivate, Router, UrlSegment, UrlTree } from '@angular/router';
import { filter, map, Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';


@Injectable({
  providedIn: 'root'
})
export class UserGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router) { }
  canActivate(): Observable<boolean> {
    return this.authService.isAuthenticatedUser.pipe(
      filter((val) => val !== null),
      //filter data untuk yg diterima saat subscribe
      map((val) => {
        //map itu buat bikin satu fungsi baru
        if (val) {
          return true
        } else {
          this.router.navigate(['/login'])
          return false
        }
      }
      )
    )
  }
}
