import { Component } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {

  usernameLog: string | null

  constructor(private userService: UserService, private authService: AuthService) {
    this.usernameLog = localStorage.getItem('username')
  }

  doLogout() {
    localStorage.clear()
  }

  toggle(){
    const element = document.body as HTMLBodyElement
    element.classList.toggle('toggle-sidebar')
  }
}
