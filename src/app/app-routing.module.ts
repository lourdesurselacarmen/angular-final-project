import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminGuard } from './guards/admin.guard';
import { UserGuard } from './guards/user.guard';
import { CalculateGasolineComponent } from './pages/calculate-gasoline/calculate-gasoline.component';
import { LoginComponent } from './pages/login/login.component';
import { PlaygroundComponent } from './pages/playground/playground.component';
// import { RegisterComponent } from './pages/register/register.component';
import { UsersComponent } from './pages/users/users.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },
  // {
  //   path: 'register',
  //   component: RegisterComponent
  // },
  {
    path: 'playground',
    component: PlaygroundComponent
  },
  {
    path: 'calculate-gasoline',
    component: CalculateGasolineComponent
  },
  {
    path: 'users',
    component: UsersComponent,
    canActivate: [UserGuard]
  },
  {
    path: 'admin',
    loadChildren: () => import('./pages/admin/admin.module').then( m => m.AdminModule),
    canLoad: [AdminGuard]
    //tipenya load jadi memisahkan admin component di app module
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
