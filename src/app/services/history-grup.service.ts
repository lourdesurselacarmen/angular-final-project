import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable } from 'rxjs';
import { ResponseBase } from '../interfaces/response.interface';
import { TransaksiGrup } from '../interfaces/transaksi.interface';

@Injectable({
  providedIn: 'root'
})
export class HistoryGrupService {

  private baseApi = 'http://localhost:8080'

  constructor(private httpClient: HttpClient) { }

  getlistTransaksi(): Observable<TransaksiGrup[]> {

    return this.httpClient.get<ResponseBase<TransaksiGrup[]>>(`${this.baseApi}/api/arisan-arisan-club/admin/history-transaksi/group`).pipe(
      map((val) => {
        return val.data;
      }),
      catchError((err) => {
        console.log(err)
        throw err
      })
    )
  }
}
