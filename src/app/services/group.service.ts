import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map, catchError } from 'rxjs';
import { ResponseBase } from '../interfaces/response.interface';
import { GroupAdmin, GroupTransaction, JumlahTransaksiGrup, MemberTransaction, RequestChangeAdmin, RequestRegisterGroup, ResponseRegisterGroup } from '../interfaces/group.interface';
import { Dompet, TransaksiGrup } from '../interfaces/transaksi.interface';

@Injectable({
  providedIn: 'root'
})
export class GroupService {

  private baseApi = 'http://localhost:8080'

  constructor(private httpClient: HttpClient) { }

  getlistGroup(): Observable<GroupTransaction[]> {

    return this.httpClient.get<ResponseBase<GroupTransaction[]>>(`${this.baseApi}/api/arisan-arisan-club/search-group`).pipe(
      map((val) => {
        return val.data;
      }),
      catchError((err) => {
        console.log(err)
        throw err
      })
    )
  }

  registerGroup(totalMoney: number, payload: RequestRegisterGroup): Observable<ResponseRegisterGroup> {
    return this.httpClient.post<ResponseRegisterGroup>(this.baseApi + '/api/arisan-arisan-club/admin/create-group/' + totalMoney, payload)
  }

  getMemberList(groupName: string): Observable<MemberTransaction[]> {
    return this.httpClient.get<ResponseBase<MemberTransaction[]>>(`${this.baseApi}/api/arisan-arisan-club/search-group-member/` + groupName).pipe(
      map((val) => {
        return val.data;
      }),
      catchError((err) => {
        console.log(err)
        throw err
      })
    )
  }

  getGroupTransaction(groupName: string): Observable<TransaksiGrup[]> {
    return this.httpClient.get<ResponseBase<TransaksiGrup[]>>(`${this.baseApi}/api/arisan-arisan-club/admin/history-transaksi/group`).pipe(
      map((val) => {
        return val.data;
      }),
      catchError((err) => {
        console.log(err)
        throw err
      })
    )
  }

  getDompetGrup(groupName: string, username: string): Observable<Dompet> {
    return this.httpClient.get<ResponseBase<Dompet>>(`${this.baseApi}/api/arisan-arisan-club/admin/getting-group-wallet/` + groupName).pipe(
      map((val) => {
        return val.data;
      }),
      catchError((err) => {
        console.log(err)
        throw err
      })
    )
  }

  changeAdminGroup(groupName: string, payload: RequestChangeAdmin): Observable<GroupAdmin[]> {
    return this.httpClient.post<GroupAdmin[]>(this.baseApi + '/api/arisan-arisan-club/change-admin-group/' + groupName, payload)
  }

  getAllTransaction(): Observable<JumlahTransaksiGrup[]> {
    return this.httpClient.get<ResponseBase<JumlahTransaksiGrup[]>>(`${this.baseApi}/api/arisan-arisan-club/admin/history-transaksi/user`).pipe(
      map((val) => {
        return val.data;
      }),
      catchError((err) => {
        console.log(err)
        throw err
      })
    )
  }
}
