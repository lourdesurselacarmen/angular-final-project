import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable } from 'rxjs';
import { ResponseBase } from '../interfaces/response.interface';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  private baseApi = 'http://localhost:8080'
  constructor(private httpClient: HttpClient) { }

  getDashboardTransaction(): Observable<number> {
    return this.httpClient.get<ResponseBase<number>>(`${this.baseApi}/api/arisan-arisan-club/getting-transaction-value`).pipe(
      map((val) => {
        return val.data;
      }),
      catchError((err) => {
        console.log(err)
        throw err
      })
    )
  }

  getDashboardRevenue(): Observable<number> {
    return this.httpClient.get<ResponseBase<number>>(`${this.baseApi}/api/arisan-arisan-club/revenue`).pipe(
      map((val) => {
        return val.data;
      }),
      catchError((err) => {
        console.log(err)
        throw err
      })
    )
  }
}
