import { TestBed } from '@angular/core/testing';

import { HistoryGrupService } from './history-grup.service';

describe('HistoryGrupService', () => {
  let service: HistoryGrupService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HistoryGrupService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
