import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, map, tap } from 'rxjs';
import { GroupList, UploadFoto, Userdata, UserList } from '../interfaces/user.interface';
import { ResponseBase } from '../interfaces/response.interface';
import { ResponseLogin, ResponseRegister } from '../interfaces/authInterface';
import { TransaksiUser, TransaksiUserMember } from '../interfaces/transaksi.interface';
import { ResponseRegisterGroup } from '../interfaces/group.interface';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseApi = 'http://localhost:8080'

  private baseApingrok = 'https://1cfe-125-164-20-251.ap.ngrok.io'

  constructor(private httpClient: HttpClient) { }

  err:any

  getlistUser(): Observable<Userdata[]> {

    // const token = localStorage.getItem('token')
    // console.log(token)

    // const headers = new HttpHeaders({
    //   'Authorization': `Bearer ${(token)}`,
    // })

    return this.httpClient.get<ResponseBase<Userdata[]>>(`${this.baseApi}/api/arisan-arisan-club/userdata`).pipe(
      map((val) => {
        
        return val.data;
      }),
      catchError((err) => {
        console.log(err)
        throw err
      })
    )
  }

  // uploadPhoto(data: File): Observable<ResponseBase<UploadFoto>> {
  //   const file = new FormData
  //   file.append('file', data, data.name)
  //   return this.httpClient.post<ResponseBase<UploadFoto>>(`${this.baseApingrok}/users/uploadPhoto`, file)
  // }

  adduser(payload: any) {
    return this.httpClient.post<ResponseBase<any>>(`${this.baseApingrok}/register`, payload)
  }

  getlistTransaksi(username: string): Observable<TransaksiUserMember[]> {

    return this.httpClient.get<ResponseBase<TransaksiUserMember[]>>(`${this.baseApi}/api/arisan-arisan-club/admin/history-transaksi/userID/` + username).pipe(
      map((val) => {
        return val.data;
      }),
      catchError((err) => {
        console.log(err)
        throw err
      })
    )
  }

  getlistGroup(username: string): Observable<ResponseRegisterGroup[]> {

    return this.httpClient.get<ResponseBase<ResponseRegisterGroup[]>>(`${this.baseApi}/api/arisan-arisan-club/admin/search-group-user/` + username).pipe(
      map((val) => {
        return val.data;
      }),
      catchError((err) => {
        console.log(err)
        throw err
      })
    )
  }

  getUserProfile() : Observable<ResponseRegister>{
    return this.httpClient.get<ResponseBase<ResponseRegister>>(`${this.baseApi}/api/arisan-arisan-club/current-user`).pipe(
      map((val) => {
        return val.data;
      }),
      catchError((err) => {
        console.log(err)
        throw err
      })
    )
  }

  updateProfil(idUser: number, payload: any){
    return this.httpClient.post<ResponseBase<ResponseRegister>>(`${this.baseApi}/api/arisan-arisan-club/userdata/ganti-profil-admin`, payload)
  }
}
