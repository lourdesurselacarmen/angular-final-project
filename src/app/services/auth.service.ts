import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, tap } from 'rxjs';
import { RequestLogin, ResponseLogin, RequestRegister, ResponseRegister } from '../interfaces/authInterface';
import { ResponseBase } from '../interfaces/response.interface';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private baseApi = 'http://localhost:8080'

  public isAuthenticated = new BehaviorSubject<boolean>(false)
  public isAuthenticatedUser = new BehaviorSubject<boolean>(false)
  //kurung siku = kelas generic jadi bisa nanem tipe data yg kita buat

  constructor(private httpClient: HttpClient) {
    this.isLogin()
  }

  login(payload: RequestLogin): Observable<ResponseBase<ResponseLogin>> {
    return this.httpClient.post<ResponseBase<ResponseLogin>>(this.baseApi + '/arisan-arisan-club/login', payload).pipe(
      tap( val => {
        console.log(val)
        if(val.data.role ==='admin') {
          this.isAuthenticated.next(true)
        } else if(val.data.role === 'user') {
          this.isAuthenticatedUser.next(true)
        }
      })
    )
  }

  register(payload: RequestRegister): Observable<ResponseBase<ResponseRegister>> {
    return this.httpClient.post<ResponseBase<ResponseRegister>>(this.baseApi + '/arisan-arisan-club/register', payload).pipe(
      tap( val => {
        console.log(val) 
      }
    )
    )
  }

  isLogin() {
    //cek tokennya ada apa engga tapi belum validasi bener apa salah
    const token = localStorage.getItem('token')
    if(token) {
      this.isAuthenticated.next(true)
    }
  }

  loginInfo() {

  }
}
