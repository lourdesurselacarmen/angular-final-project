import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../interfaces/interface';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  public baseHarga = 10000

  private baseApi = 'https://jsonplaceholder.typicode.com'

  constructor(private httpClient: HttpClient) {}

  //kalo ga ditulis private dia otomatis public
    getUser(): Observable<User[]> {
      //di user nambah bracket array karna dia bukan 1 aja isi objeknya
      return this.httpClient.get<User[]>(this.baseApi + '/users')
      //yg di belakang tambah itu root setelah api dasarnya
    }
}