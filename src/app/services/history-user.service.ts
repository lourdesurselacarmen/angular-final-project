import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable } from 'rxjs';
import { MemberTransaction } from '../interfaces/group.interface';
import { ResponseBase } from '../interfaces/response.interface';
import { MemberTransactionList } from '../interfaces/transaksi.interface';

@Injectable({
  providedIn: 'root'
})
export class HistoryUserService {

  private baseApi = 'http://localhost:8080'

  constructor(private httpClient: HttpClient) { }

  getlistTransaksi(): Observable<MemberTransactionList[]> {

    return this.httpClient.get<ResponseBase<MemberTransactionList[]>>(`${this.baseApi}/api/arisan-arisan-club/all-member`).pipe(
      map((val) => {
        return val.data;
      }),
      catchError((err) => {
        console.log(err)
        throw err
      })
    )
  }
}
